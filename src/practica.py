# Función que suma dos enteros
def funcion1(num1, num2):
    suma = num1 + num2
    print(f"La suma de {num1} y {num2} es {suma}")

# Función que multiplica dos números decimales
def funcion2(num1, num2):
    mult = num1 * num2
    print(f"El producto de {num1} y {num2} es {mult}")


# Función que concatena dos cadenas de texto
def funcion3(str1, str2):
    concatenada = str1 + str2
    print(f"Las dos cadenas {str1} y {str2} concatenadas producen {concatenada}")

# Función que verifica si un número entero es par
def funcion4(numero):
    if numero%2==0:
        print(f"{numero} es par.")
    else:
        print(f"{numero} no es par.")


# Función que calcula el área de un triángulo dado su base y altura
def funcion5(base, altura):
    area = (base*altura)/2
    print(f"El área del triángulo con base {base} y altura {altura} es de {area}")
